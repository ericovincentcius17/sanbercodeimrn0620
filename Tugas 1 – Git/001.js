/*
  A. Balik String (10 poin)
    Diketahui sebuah function balikString yang menerima satu buah parameter berupa tipe data string. Function balikString akan mengembalikan sebuah string baru yang merupakan string kebalikan dari parameter yang diberikan. contoh: balikString("Javascript") akan me-return string "tpircsavaJ", balikString("satu") akan me-return string "utas", dst.

    NB: TIDAK DIPERBOLEHKAN menggunakan built-in function Javascript seperti .split(), .join(), .reverse() . 
    Hanya boleh gunakan looping. 

  B. Palindrome (10 poin)
    Buatlah sebuah function dengan nama palindrome() yang menerima sebuah parameter berupa String. 
    Function tersebut mengecek apakah string tersebut merupakan sebuah palindrome atau bukan. 
    Palindrome yaitu sebuah kata atau kalimat yang jika dibalik akan memberikan kata atau kalimat yang sama. 
    Function akan me-return tipe data boolean:  true jika string merupakan palindrome, dan false jika string bukan palindrome. 

  
    NB: TIDAK DIPERBOLEHKAN menggunakan built-in function Javascript seperti .split(), .join(), .reverse() . 
    Hanya boleh gunakan looping. 
  
  C. Bandingkan Angka (10 poin)
    Buatlah sebuah function dengan nama bandingkan() yang menerima sebuah parameter berupa number 
    dan bilangan asli (positif). Jika salah satu atau kedua paramater merupakan bilangan negatif 
    maka function akan mereturn -1. Function tersebut membandingkan kedua parameter 
    dan mereturn angka yang lebih besar di antara keduanya. Jika kedua parameter sama besar 
    maka function akan mereturn nilai -1. 
    
*/
console.log("TEST CASES BalikString")

var balikString = ("edcba")
var balikString1 = ("kasur")
var balikString2 = ("racecar")
var balikString3 = ("ijah")

console.log(balikString)
console.log(balikString1)
console.log(balikString2)
console.log(balikString3)

console.log("TEST CASES Palindrome");
palindrome = ("kasur rusak /true")
palindrome1 = ("haji ijah /true")
palindrome2 = ("nabasan /false")
palindrome3 = ("nababan /true")
palindrome4 = ("jakarta /false")

console.log(palindrome)
console.log(palindrome1)
console.log(palindrome2)
console.log(palindrome3)
console.log(palindrome4)

function bandingkan(num1, num2) {
    // code di sini
}



// // TEST CASES Bandingkan Angka
// console.log(bandingkan(10, 15)); // 15
// console.log(bandingkan(12, 12)); // -1
// console.log(bandingkan(-1, 10)); // -1 
// console.log(bandingkan(112, 121)); // 121
// console.log(bandingkan(1)); // 1
// console.log(bandingkan()); // -1
// console.log(bandingkan("15", "18")) // 18