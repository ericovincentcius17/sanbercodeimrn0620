var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

function readBooks(time, book, callback) {
    console.log(`saya membaca ${book.name}`)
    setTimeout(function(time) {
        let sisaWaktu = 0
        if (time > books.timeSpent) {
            sisaWaktu = time - books.timeSpent
            console.log("saya sudah membaca ${books.name}, sisa waktu saya ${sisaWaktu}")
            callback(sisaWaktu)
        } else {
            console.log('waktu saya habis')
            callback(time)
        }
    }, books.timeSpent)
}

module.exports = readBooks

console.log({ name: 'LOTR', timeSpent: 3000 }, { name: 'Fidas', timeSpent: 2000 }, { name: 'Kalkulus', timeSpent: 4000 });