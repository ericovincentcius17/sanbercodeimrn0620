import React from "react";
import { StyleSheet, Text, View } from "react-native";

import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import 'react-native-gesture-handler';
export default function App() {
    return ( <
        View style = { styles.container } >
        <
        Text > Open up App.js to start working on your app! < /Text> <
        /View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center"
    }
});